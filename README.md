# Hugo Site

## Build website
```
hugo
```

## Serve website locally (with quick change detection for development)
```
hugo server -D
```

## Using archetypes (from . directory)
```
hugo new articles/new-article-post.md
hugo new news/new-news-post.md
hugo new serials/new-serial-post.md
```