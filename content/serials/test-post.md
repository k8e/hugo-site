---
title: "Test Post"
date: 2019-06-11T23:22:14-07:00
draft: false
tags:
- test
- test2

---

# Test Post 1

### Batch Processing

The earliest computers were extremely expensive devices, and very slow in comparison to later models. Machines were typically dedicated to a particular set of tasks and operated by control panels, the operator manually entering small programs via switches in order to load and run a series of programs. These programs might take hours, or even weeks, to run. As computers grew in speed, run times dropped, and soon the time taken to start up the next program became a concern. Batch processing methodologies evolved to decrease these "dead periods" by queuing up programs so that as soon as one program completed, the next would start.

To support a batch processing operation, a number of comparatively inexpensive card punch or paper tape writers were used by programmers to write their programs "offline". When typing (or punching) was complete, the programs were submitted to the operations team, which scheduled them to be run. Important programs were started quickly; how long before less important programs were started was unpredictable[citation needed]. When the program run was finally completed, the output (generally printed) was returned to the programmer. The complete process might take days, during which time the programmer might never see the computer.

The alternative of allowing the user to operate the computer directly was generally far too expensive to consider. This was because users might have long periods of entering code while the computer remained idle. This situation limited interactive development to those organizations that could afford to waste computing cycles: large universities for the most part. Programmers at the universities decried the behaviors that batch processing imposed, to the point that Stanford students made a short film humorously critiquing it.[3] They experimented with new ways to interact directly with the computer, a field today known as human–computer interaction. 

### Time-Sharing

Time-sharing was developed out of the realization that while any single user would make inefficient use of a computer, a large group of users together would not. This was due to the pattern of interaction: Typically an individual user entered bursts of information followed by long pauses but a group of users working at the same time would mean that the pauses of one user would be filled by the activity of the others. Given an optimal group size, the overall process could be very efficient. Similarly, small slices of time spent waiting for disk, tape, or network input could be granted to other users.

The concept is claimed to have been first described by John Backus in the 1954 summer session at MIT,[4] and later by Bob Bemer in his 1957 article "How to consider a computer" in Automatic Control Magazine.[5][6] In a paper published in December 1958 by W. F. Bauer,[7] he wrote that "The computers would handle a number of problems concurrently. Organizations would have input-output equipment installed on their own premises and would buy time on the computer much the same way that the average household buys power and water from utility companies."

Implementing a system able to take advantage of this was initially difficult.[1][8][9] Batch processing was effectively a methodological development on top of the earliest systems. Since computers still ran single programs for single users at any time, the primary change with batch processing was the time delay between one program and the next. Developing a system that supported multiple users at the same time was a completely different concept. The "state" of each user and their programs would have to be kept in the machine, and then switched between quickly. This would take up computer cycles, and on the slow machines of the era this was a concern. However, as computers rapidly improved in speed, and especially in size of core memory in which users' states were retained, the overhead of time-sharing continually decreased, relatively speaking.

The first project to implement time-sharing of user programs was initiated by John McCarthy at MIT in 1959, initially planned on a modified IBM 704, and later on an additionally modified IBM 709 (one of the first computers powerful enough for time-sharing).[9] One of the deliverables of the project, known as the Compatible Time-Sharing System or CTSS, was demonstrated in November 1961. CTSS has a good claim to be the first time-sharing system and remained in use until 1973. Another contender for the first demonstrated time-sharing system was PLATO II, created by Donald Bitzer at a public demonstration at Robert Allerton Park near the University of Illinois in early 1961. But this was a special purpose system. Bitzer has long said that the PLATO project would have gotten the patent on time-sharing if only the University of Illinois had not lost the patent for 2 years.[10] JOSS began time-sharing service in January 1964.[11]

The first commercially successful time-sharing system was the Dartmouth Time Sharing System.

### Development

Throughout the late 1960s and the 1970s, computer terminals were multiplexed onto large institutional mainframe computers (centralized computing systems), which in many implementations sequentially polled the terminals to see whether any additional data was available or action was requested by the computer user. Later technology in interconnections were interrupt driven, and some of these used parallel data transfer technologies such as the IEEE 488 standard. Generally, computer terminals were utilized on college properties in much the same places as desktop computers or personal computers are found today. In the earliest days of personal computers, many were in fact used as particularly smart terminals for time-sharing systems.

The Dartmouth Time Sharing System's creators wrote in 1968 that "any response time which averages more than 10 seconds destroys the illusion of having one's own computer".[13] Conversely, timesharing users thought that their terminal was the computer.[14]

With the rise of microcomputing in the early 1980s, time-sharing became less significant, because individual microprocessors were sufficiently inexpensive that a single person could have all the CPU time dedicated solely to their needs, even when idle.

However, the Internet brought the general concept of time-sharing back into popularity. Expensive corporate server farms costing millions can host thousands of customers all sharing the same common resources. As with the early serial terminals, web sites operate primarily in bursts of activity followed by periods of idle time. This bursting nature permits the service to be used by many customers at once, usually with no perceptible communication delays, unless the servers start to get very busy. 

### Time-Sharing Business

n the 1960s, several companies started providing time-sharing services as service bureaus. Early systems used Teletype Model 33 KSR or ASR or Teletype Model 35 KSR or ASR machines in ASCII environments, and IBM Selectric typewriter-based terminals (especially the IBM 2741) with two different seven-bit codes.[15] They would connect to the central computer by dial-up Bell 103A modem or acoustically coupled modems operating at 10–15 characters per second. Later terminals and modems supported 30–120 characters per second. The time-sharing system would provide a complete operating environment, including a variety of programming language processors, various software packages, file storage, bulk printing, and off-line storage. Users were charged rent for the terminal, a charge for hours of connect time, a charge for seconds of CPU time, and a charge for kilobyte-months of disk storage.

Common systems used for time-sharing included the SDS 940, the PDP-10, and the IBM 360. Companies providing this service included GE's GEISCO, IBM subsidiary The Service Bureau Corporation, Tymshare (founded in 1966), National CSS (founded in 1967 and bought by Dun & Bradstreet in 1979), Dial Data (bought by Tymshare in 1968), Bolt, Beranek, and Newman (BBN) and Time Sharing Ltd. in the U.K.. By 1968, there were 32 such service bureaus serving the US National Institutes of Health (NIH) alone.[16] The Auerbach Guide to Timesharing (1973) lists 125 different timesharing services using equipment from Burroughs, CDC, DEC, HP, Honeywell, IBM, RCA, Univac, and XDS.

